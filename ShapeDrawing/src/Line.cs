﻿using System;
using SwinGameSDK;
using System.IO;

namespace MyGame
{
	public class Line : Shape
	{
		float _endX;
		float _endY;

		public Line (Color clr, float starX, float starY, float endX, float endY) : base (clr)
		{
			_endX = endX;
			_endY = endY;
			X = starX;
			Y = starY;
			Color = clr;
		}

		public Line () : this (Color.OrangeRed, 0, 0, 100, 100)
		{
			
		}

		public override void LoadFrom (StreamReader reader)
		{
			base.LoadFrom (reader);
			EndX = reader.ReadInteger ();
			EndY = reader.ReadInteger ();
		}

		public override void SaveTo (StreamWriter writer)
		{
			//writer.WriteLine ("Line");
			base.SaveTo (writer);
			writer.WriteLine (EndX);
			writer.WriteLine (EndY);
		}

		public override void Draw ()
		{
			if (Selected) DrawOutline ();
			SwinGame.DrawLine (Color, X, Y, X + _endX, Y + _endY);
		}

		public override void DrawOutline ()
		{
			SwinGame.DrawCircle (Color.Black, X, Y, 10);
			SwinGame.DrawCircle (Color.Black, X +_endX, Y + _endY, 10);
		}

		public override bool IsAt (Point2D pt)
		{
			return SwinGame.PointOnLine (pt, X, Y, _endX, _endY);
		}

		public float EndX {
			get {
				return _endX;
			}
			set {
				_endX = value;
			}
		}

		public float EndY {
			get {
				return _endY;
			}
			set {
				_endY = value;
			}
		}

	}
}
