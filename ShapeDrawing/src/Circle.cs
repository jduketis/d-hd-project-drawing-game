﻿using System;
using SwinGameSDK;
using System.IO;

namespace MyGame
{
	public class Circle : Shape
	{
		private int _radius;

		public Circle (Color clr, int radius) : base (clr)
		{
			Radius = radius;
			Color = clr;
		}

		public Circle () : this (Color.Blue, 50)
		{
		}

		public override void LoadFrom (StreamReader reader)
		{
			base.LoadFrom (reader);
			Radius = reader.ReadInteger ();
		}

		public override void SaveTo (StreamWriter writer)
		{
			//writer.WriteLine ("Circle");
			base.SaveTo (writer);
			writer.WriteLine (Radius);
		}

		public override void Draw ()
		{
			if (Selected) DrawOutline ();
			SwinGame.FillCircle (Color, X, Y, _radius);
		}

		public override void DrawOutline ()
		{
			SwinGame.DrawCircle (Color.Black, X, Y, _radius + 2);
		}

		public override bool IsAt (Point2D pt)
		{
			return SwinGame.PointInCircle (pt, X, Y, Radius);
		}

		public int Radius {
			get {
				return _radius;
			}
			set {
				_radius = value;
			}
		}
	}
}
