﻿using System;
using SwinGameSDK;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace MyGame
{

    //Checked by Jonathan Duketis - 6883826
    //Shapes are all working - rectangles, circles and lines.

	public abstract class Shape
	{
		private Color _color;
		private float _x, _y;
		private bool _selected;

		private static Dictionary<string, Type> _ShapeClassRegistry = new Dictionary<string, Type> ();

		public static void RegisterShape (string name, Type t)
		{
			_ShapeClassRegistry [name] = t;
		}

		public static Shape CreateShape (string name)
		{
			return (Shape)Activator.CreateInstance (_ShapeClassRegistry [name]);
		}

		public Shape (Color _color)
		{
		}

		public Shape () : this (Color.Yellow)
		{
		}

		public static string Key (Type value)
		{
			int i = -1;
			List<string> keyValues = _ShapeClassRegistry.Keys.ToList ();

			if (_ShapeClassRegistry.Values.Contains (value)) {
				i = _ShapeClassRegistry.Values.ToList ().IndexOf (value);
			}

			if (i >= 0) {
				return keyValues.ElementAt (i);
			} else {
				return null;
			}
		}
		//LOAD FEATURE Code checked and written by Jonathan Duketis - 6883826
		//OUTCOME: WORKING FINE ! :) 

		public virtual void LoadFrom (StreamReader reader)
		{
			Color = Color.FromArgb (reader.ReadInteger ());
			X = reader.ReadInteger ();
			Y = reader.ReadInteger ();
		}
		//Code checked and written by Jonathan Duketis - 6883826
        //OUTCOME: WORKING FINE ! :) 

		public virtual void SaveTo (StreamWriter writer)
		{
			writer.WriteLine (Key(this.GetType()));
			writer.WriteLine (Color.ToArgb ());
			writer.WriteLine (X);
			writer.WriteLine (Y);
		}
 
		public abstract void Draw ();

		public abstract void DrawOutline ();

		public abstract bool IsAt (Point2D pt);

		public Color Color {
			get {
				return _color;
			}
			set {
				_color = value;
			}
		}

		public bool Selected {
			get {
				return _selected;
			}
			set {
				_selected = value;
			}
		}

		public float X {
			get {
				return _x;
			}
			set {
				_x = value;
			}
		}

		public float Y {
			get {
				return _y;
			}
			set {
				_y = value;
			}
		}
	}
}
