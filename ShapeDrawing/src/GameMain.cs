using System;
using SwinGameSDK;

namespace MyGame
{
    public class GameMain
	{
		private enum Shapekind
		{
			Rectangle,
			Circle,
			Line
		}

        public static void Main()
		{
			Shape.RegisterShape ("Rectangle", typeof (Rectangle));
			Shape.RegisterShape ("Circle", typeof (Circle));
			Shape.RegisterShape ("Line", typeof (Line));

			//Start the audio  system so sound can be played
			SwinGame.OpenAudio ();

			//Open the game window

			//Jonathan Duketis Fix - Start Game Window 
			//OUTCOME: Bug Eliminated.! 

			SwinGame.OpenGraphicsWindow("GameMain", 800, 600);
            SwinGame.ShowSwinGameSplashScreen();

			Shapekind kindToAdd;
			kindToAdd = Shapekind.Circle;

			Drawing drawing = new Drawing ();

			//Run the game loop
			while (false == SwinGame.WindowCloseRequested ()) {
				//Fetch the next batch of UI interaction
				SwinGame.ProcessEvents ();
				SwinGame.DrawFramerate (0, 0);
				drawing.Draw ();

				//add a shape
				if (SwinGame.MouseClicked (MouseButton.LeftButton)) {

					Shape newShape;

					if (kindToAdd == Shapekind.Circle) {
						Circle newCircle = new Circle ();
						newShape = newCircle;
					} else if (kindToAdd == Shapekind.Rectangle) {
						Rectangle newRect = new Rectangle ();
						newShape = newRect;
					} else {
						Line newLine = new Line ();
						newShape = newLine;
					}

					newShape.X = SwinGame.MouseX ();
					newShape.Y = SwinGame.MouseY ();
					drawing.AddShape (newShape);
				}

				//change the background colour
				if (SwinGame.KeyTyped (KeyCode.SpaceKey)) {
					drawing.Background = SwinGame.RandomRGBColor (alpha: 255);
				}

				//selects a shape
				if (SwinGame.MouseClicked (MouseButton.RightButton)) {
					drawing.SelectShapesAt (SwinGame.MousePosition ());
				}

				//removes a selected shape
				if (SwinGame.KeyTyped (KeyCode.BackspaceKey) || SwinGame.KeyTyped (KeyCode.DeleteKey)) {
					foreach (Shape s in drawing.SelectedShapes) {
						drawing.RemoveShape (s);
					}
				}

				//switches shape to rectangle
				if (SwinGame.KeyTyped (KeyCode.RKey)) {
					kindToAdd = Shapekind.Rectangle;
				}

				//switches shape to circle
				if (SwinGame.KeyTyped (KeyCode.CKey)) {
					kindToAdd = Shapekind.Circle;
				}

				//switches shape to line
				if (SwinGame.KeyTyped (KeyCode.LKey)) {
					kindToAdd = Shapekind.Line;
				}

				//Saves Game
				if (SwinGame.KeyTyped (KeyCode.SKey)) {
					drawing.Save ("/Users/Jonathan/Library/Mobile Documents/com~apple~CloudDocs/Documents/Uni/Computer Science/Object Oriented Programming/5.3d");
				}

				//Loads Game
				if (SwinGame.KeyTyped (KeyCode.OKey)) {
					try {
						drawing.Load ("/Users/Jonathan/Library/Mobile Documents/com~apple~CloudDocs/Documents/Uni/Computer Science/Object Oriented Programming/5.3d");
					} catch (Exception e) {
						Console.Error.WriteLine ("Error Loading File: {0}", e.Message);
					}
				}

				SwinGame.RefreshScreen (60);
   	      }
        }
    }
}