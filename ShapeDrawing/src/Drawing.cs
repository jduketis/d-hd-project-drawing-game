﻿using System;
using SwinGameSDK;
using System.Collections.Generic;
using System.IO;

namespace MyGame
{
	public class Drawing
	{
		private readonly List<Shape> _shapes;
		private Color _background;

		public Drawing (Color background)
		{
			_shapes = new List<Shape> ();
			_background = background;
		}

		public Drawing () : this (Color.White)
		{
		}

		public void Save (string filename)
		{
			StreamWriter writer;
			writer = new StreamWriter (filename);
			try 
			{
				writer.WriteLine (Background.ToArgb ());
				writer.WriteLine (ShapeCount);

				foreach (Shape s in _shapes) {
					s.SaveTo (writer);
				}
			} 
			finally 
			{
				writer.Close ();
			}
		}

		public void Load (string filename)
		{
			StreamReader reader;
			int count;
			Shape s;
			string kind;

			reader = new StreamReader (filename);
			try {
				Background = Color.FromArgb (reader.ReadInteger ());
				count = reader.ReadInteger ();

				for (int i = 0; i < count; i++) {

					kind = reader.ReadLine ();
					try {
						s = Shape.CreateShape (kind);
					}
					catch {
						throw new InvalidDataException ("Unknown Shape Kind: " + kind);
					}

					s.LoadFrom (reader);
					AddShape (s);
				}
			} 
			finally 
			{
				reader.Close ();
			}
		}

		public void Draw ()
		{
			SwinGame.ClearScreen (Background);
			foreach (Shape s in _shapes) {
				s.Draw ();
				if (s.Selected) {
					s.DrawOutline ();
				}
			}
		}

		public void AddShape (Shape s)
		{
			_shapes.Add (s);
		}

		public void RemoveShape (Shape s)
		{
			_shapes.Remove (s);	
		}

		public void SelectShapesAt (Point2D pt)
		{
			foreach (Shape s in _shapes) {
				s.Selected = s.IsAt (SwinGame.MousePosition ());
			}
		}

		public List<Shape> SelectedShapes {
			get {
				List<Shape> result = new List<Shape> ();
				foreach (Shape s in _shapes) {
					if (s.Selected) {
						result.Add (s);
					}
				}
				return result;
			}
		}
			
		public int ShapeCount  {
			get { return _shapes.Count; }
		}

		public Color Background  {
			get {
				return _background;
			}
			set {
				_background = value;
			}
		}
	}
}
