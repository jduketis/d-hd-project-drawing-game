# Meeting Minutes
** Date: 11/11/2017
** Start Time: 10:00AM
** End Time: 10:30AM
** Location: Swinburne University Of Technology
** Attendees: Jonathan Duketis - 6883826


# Agenda Items

Discuss the development of an already existing drawing game using the SwinGame Engine 

Tasks Discussed:

** Task Management Tools Used For Projects
	+ Outcome: Trello and Visual Studio for Running Game

** Version control that will be used is Bitbucket

 ** Get Program Working from initial bugs and get screen working
 ** Add further Shapes like circle, rectangle and lines
 ** Add Save Feature so you can save your drawing
 ** Add Load Feature so you can load drawings from txt file

** Meeting Minutes will be stored in the repository. 
	+ Jonathan will push and commit the changes for all work.

